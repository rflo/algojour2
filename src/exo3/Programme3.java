package exo3;

import java.util.Scanner;

public class Programme3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
// exo1 : Programme qui additionne les valeurs d'un tableau

		int [] tabNb = {15,1,3,10,5,8,1};

		int somme = 0;
		for (int index : tabNb) {
			somme += index;
		}
		System.out.println("La somme du tableau est " + somme);

	}

}
