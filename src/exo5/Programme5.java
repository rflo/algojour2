package exo5;

import java.util.Scanner;

public class Programme5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// exo1 : Programme qui demande de remplir saisir une valeur et la compare a
		// celles d'un tableau
		int[] tabNb = { 1, 5, 10, 15, 20 };

		System.out.println("Merci de saisir un chiffre");
		Scanner scanner = new Scanner(System.in);
		int valeur = scanner.nextInt();
		
		System.out.println(contains(tabNb,valeur));
	}

	public static boolean contains(int[] array, int item) {
		for (int i : array) {
			if (item == i) {
				return true;
			}
		}
		return false;
	}
}
