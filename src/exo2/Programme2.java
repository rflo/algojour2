package exo2;

import java.util.Arrays;
import java.util.Scanner;

public class Programme2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
// exo1 : programme qui trie deux tableaux . 1 de chiffres et 1 de lettres

		int [] tabNb = new int[5];
		String [] tabCar = new String[5];
		
		tabNb[0] = 10;
		tabNb[1] = 15;
		tabNb[2] = 2;
		tabNb[3] = 55;
		tabNb[4] = 0;

		tabCar[0] = "Blabla car";
		tabCar[1] = "Blablacar";
		tabCar[2] = "Arriv�e bal";
		tabCar[3] = "Zo� java";
		tabCar[4] = "Barbapapa;";
		
		Arrays.sort(tabNb); 
		System.out.println(Arrays.toString(tabNb));
		Arrays.sort(tabCar); 
		System.out.println(Arrays.toString(tabCar));

	}

}
